
use std::fs::File;
use std::io::BufReader;

extern crate xml;
use xml::reader::{EventReader, XmlEvent};

extern crate chrono;
use chrono::DateTime;

#[derive(Clone, Copy)]
pub struct Point {
    pub lat: f64,
    pub lon: f64,
    pub elv: Option<f64>,
    pub time: Option<i64>,
    pub speed: Option<f32>,
}

pub fn gpx_parse_points_elv_time_speed(path: &str) -> Vec<Point> {
    let mut points = vec![];
    let empty_vector = vec![];
    let parser = get_gpx_parser_from_path(path);
    let mut current_element = String::new();
    let mut pt = Point { lat: 0.0, lon: 0.0, elv: None , time: None, speed: None};
    let mut lat_seen = false;
    let mut lon_seen = false;
    let mut elv_expected: Option<bool> = None;
    let mut time_expected: Option<bool> = None;
    let mut speed_expected: Option<bool> = None;
    let mut parsing_trkpt: bool = false;

    for e in parser {
        match e {
            Ok(XmlEvent::StartElement { name, namespace: _, attributes }) => {
                if name.local_name == "trkpt" {
                    parsing_trkpt = true;
                    for a in attributes {
                        if a.name.local_name == "lat" {
                            // println!("Latitude: {}", a.value);
                            pt.lat = a.value.parse().unwrap();
                            lat_seen = true;
                        }
                        else if a.name.local_name == "lon" {
                            // println!("Longitude: {}", a.value);
                            pt.lon = a.value.parse().unwrap();
                            lon_seen = true;
                        }
                    }
                }
                current_element = name.local_name;
            }

            Ok(XmlEvent::Characters(s)) => {
                if current_element == "ele" && parsing_trkpt == true {
                    // elv_expected should only be None until we have finished checking the first
                    // 'trkpt' element. If we see elevation data in the first trkpt then we will
                    // expect to see it in the rest.
                    if elv_expected == None {
                        elv_expected = Some(true);
                        pt.elv = Some(s.parse().unwrap());
                    }
                    // If we're expecting an elevation add it to the current point.
                    else if elv_expected == Some(true) {
                        pt.elv = Some(s.parse().unwrap());
                    }
                    else {
                        // Ignore unexpected elevation.
                        println!("Unexpected elevation data in GPX file. If any point has elevation data they all should");
                    }
                }

                if current_element == "time" && parsing_trkpt == true {
                    // time_expected should only be None until we have finished checking the first
                    // 'trkpt' element. If we see time data in the first trkpt then we will expect to
                    // see it in the rest.
                    if time_expected == None {
                        time_expected = Some(true);
                        pt.time = time_string_to_timestamp(&s);
                    }
                    // If we're expecting a time add it to the current point.
                    else if time_expected == Some(true) {
                        pt.time = time_string_to_timestamp(&s);
                    }
                    else
                    {
                        // Ignore unexpected elevation.
                        println!("Unexpected time data in GPX file. If any point has time data they all should");
                    }
                }

                if current_element == "speed" && parsing_trkpt == true {
                    // speed_expected should only be None until we have finished checking the first
                    // 'trkpt' element. If we see speed data in the first trkpt then we will expect to
                    // see it in the rest.
                    if speed_expected == None {
                        speed_expected = Some(true);
                        pt.speed = Some(s.parse().unwrap());
                    }
                    // If we're expecting a speed add it to the current point.
                    else if speed_expected == Some(true) {
                        pt.speed = Some(s.parse().unwrap());
                    }
                    else {
                        // Ignore unexpected elevation.
                        println!("Unexpected speed data in GPX file. If any point has time data they all should");
                    }
                }
            }

            Ok(XmlEvent::EndElement { name }) => {
                if name.local_name == "trkpt" {
                    if (lat_seen == false) || (lon_seen == false)
                    || ((elv_expected == Some(true)) && (pt.elv == None))
                    || ((time_expected == Some(true)) && (pt.time == None))
                    || ((speed_expected == Some(true)) && (pt.speed == None)) {
                        println!("GPX Error invalid trackpoint.");
                        return empty_vector;
                    }
                    else {
                        parsing_trkpt = false;
                        points.push(pt);
                    }
                }
            }

            Err(e) => {
                println!("Error: {}", e);
                return empty_vector;
            }
            _ => {}
        }
    }
    return points;
}

fn get_gpx_parser_from_path(path: &str) -> EventReader<BufReader<File>> {
    let file = File::open(path).unwrap();
    let file = BufReader::new(file);
    return EventReader::new(file);
}

fn time_string_to_timestamp(s: &str) -> Option<i64>
{
    let time = DateTime::parse_from_rfc3339(s);
    match time {
        Ok(t) => {
            return Some(t.timestamp());
        }
        Err(e) => {
            panic!("error parsing time: {}", e);
        }
    }
    //if s.contains(".") {
    //    match parse(s, "%Y-%m-%dT%H:%M:%S.%fZ",) {
    //        Ok(t) => return Some(t.to_timespec()),

//            Err(_) => return None,
//        }
//    }
//    else {
//        match parse(s, "%Y-%m-%dT%H:%M:%SZ") {
//            Ok(t) => return Some(t.to_timespec()),
//
//            Err(_) => return None,
//        }
//    }

}
